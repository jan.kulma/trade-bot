<?php
/**
 * Created by PhpStorm.
 * User: jan
 * Date: 12-07-2018
 * Time: 22:13
 */

namespace App\Helpers;

use Illuminate\Redis\RedisManager;

/**
 * General redis helper.
 *
 *
 * Symbols keys:
 *
 * XXX/BTC.baseVolume
 * XXX/BTC.1hClosingPrices (list)
 * XXX/BTC.15mClosingPrices (list)
 * XXXBTC.currentPrice
 *
 * Trades keys:
 *
 * ID.highestPrice
 * ID.lowestPrice
 * ID.takeProfitReached
 */
class Redis
{
    public const SYMBOL_CURRENT_PRICE      = 'currentPrice';
    public const SYMBOL_BASE_VOLUME        = 'baseVolume';
    public const SYMBOL_1H_CLOSING_PRICES  = '1hClosingPrices';
    public const SYMBOL_15M_CLOSING_PRICES = '15mClosingPrices';
    public const TRADE_HIGHEST_PRICE       = 'highestPrice';
    public const TRADE_LOWEST_PRICE        = 'lowestPrice';
    public const TRADE_TAKE_PROFIT_REACHED = 'takeProfitReached';
    public const COMMAND_LAST_TIME_RUN     = 'lastTimeRun';
    public const GLUE                      = '.';

    /**
     * @var RedisManager
     */
    public $manager;

    /**
     * Redis constructor.
     * @param RedisManager $manager
     */
    public function __construct(RedisManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $symbol
     * @return string|null $price
     */
    public function getCurrentSymbolPrice(string $symbol)
    {
        $key = $this->getKey(self::SYMBOL_CURRENT_PRICE, $symbol);
        return $this->manager->get($key);
    }

    /**
     * @param string $symbol
     * @param string $price
     */
    public function setCurrentSymbolPrice(string $symbol, string $price)
    {
        $key = $this->getKey(self::SYMBOL_CURRENT_PRICE, $symbol);
        $this->manager->set($key, $price);
    }

    /**
     * @param string $symbol
     * @param int $volume
     */
    public function setSymbolBaseVolume(string $symbol, int $volume)
    {
        $key = $this->getKey(self::SYMBOL_BASE_VOLUME, $symbol);
        $this->manager->set($key, $volume);
    }

    /**
     * @param string $symbol
     * @param array $klines
     * @param string $timeframe
     */
    public function setSymbolClosingPrices(string $symbol, array $klines, string $timeframe = '1h')
    {
        switch ($timeframe) {
            case '15m':
                $field = self::SYMBOL_15M_CLOSING_PRICES;
                break;
            default:
                $field = self::SYMBOL_1H_CLOSING_PRICES;
        }

        $key = $this->getKey($field, $symbol);
        $this->manager->del($key);

        $closingPrices = [];
        foreach ($klines as $kline) {
            // creating reversed array
            array_unshift($closingPrices, $kline[4]);
        }

        // pushes items to the front of array
        $this->manager->lpush($key, $closingPrices);
    }

    /**
     * @param string $command
     * @return string|null
     */
    public function getCommandLastTimeRun(string $command)
    {
        $key = $this->getKey(self::COMMAND_LAST_TIME_RUN, $command);
        return $this->manager->get($key);
    }

    /**
     * @param string $command
     * @param string $date
     */
    public function setCommandLastTimeRun(string $command, string $date)
    {
        $key = $this->getKey(self::COMMAND_LAST_TIME_RUN, $command);
        $this->manager->set($key, $date);
    }

    /**
     * @param $symbol
     * @return string|null
     */
    public function getBaseVolume($symbol)
    {
        $key = $this->getKey(self::SYMBOL_BASE_VOLUME, $symbol);
        return $this->manager->get($key);
    }

    /**
     * @param string $field
     * @param string $id
     * @return string
     */
    private function getKey(string $field, string $id)
    {
        return $id . self::GLUE . $field;
    }

    /**
     * @param string $pattern
     * @param null $cursor
     * @param array $allResults
     * @return array
     */
    public function getKeysByPattern(string $pattern, $cursor = null, $allResults = [])
    {
        // Zero means full iteration
        if ($cursor === '0') {
            return $allResults;
        }

        // No $cursor means init
        if ($cursor === null) {
            $cursor = '0';
        }

        // The call
        $result = $this->manager->scan($cursor, 'match', $pattern);

        // Append results to array
        $allResults = array_merge($allResults, $result[1]);

        // Recursive call until cursor is 0
        return $this->getKeysByPattern($pattern, $result[0], $allResults);
    }
}