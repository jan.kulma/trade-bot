<?php
/**
 * Created by PhpStorm.
 * User: jan
 * Date: 12-07-2018
 * Time: 23:13
 */

namespace App\Helpers;

use App\Models\Trade;

class Decision
{
    /**
     * @var Price
     */
    private $priceHelper;

    /**
     * @var Redis
     */
    private $redis;

    /**
     * Decision constructor.
     * @param Price $priceHelper
     * @param Redis $redis
     */
    public function __construct(Price $priceHelper, Redis $redis)
    {
        $this->priceHelper = $priceHelper;
        $this->redis       = $redis;
    }

    /**
     * @param Trade $trade
     * @return bool
     */
    public function keep(Trade $trade): bool
    {
//        return false;
        return !$this->takeProfitReached($trade);
    }

    /**
     * @param Trade $trade
     * @return bool
     */
    private function takeProfitReached(Trade $trade): bool
    {
        return $trade->take_profit <= $this->priceHelper->getCurrentPriceToBuyPriceInPercent($trade);
    }
}