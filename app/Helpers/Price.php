<?php
/**
 * Created by PhpStorm.
 * User: jan
 * Date: 12-07-2018
 * Time: 23:32
 */

namespace App\Helpers;


class Price
{
    /**
     * @var Redis
     */
    private $redisHelper;

    public function __construct(Redis $redisHelper)
    {
        $this->redisHelper = $redisHelper;
    }

    public function getDifferenceInPercent($a, $b)
    {
        if (!$b) return 0;

        return number_format(
            (($a - $b) / $b) * 100, 3
        );
    }

    public function getSellPriceToBuyPriceInPercent(\App\Models\Trade $trade)
    {
        return $this->getDifferenceInPercent($trade->sell_price, $trade->buy_price);
    }

    public function getCurrentPriceToBuyPriceInPercent(\App\Models\Trade $trade)
    {
        return $this->getDifferenceInPercent(
            $this->redisHelper->getCurrentSymbolPrice($trade->pair), $trade->buy_price
        );
    }
}