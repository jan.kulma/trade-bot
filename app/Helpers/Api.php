<?php
/**
 * Created by PhpStorm.
 * User: jan
 * Date: 12-07-2018
 * Time: 22:08
 */

namespace App\Helpers;

use App\Events\TradeBought;
use App\Events\TradeSold;
use App\Models\Trade;
use ccxt\binance;

class Api extends binance
{
    /**
     * ccxt needs to have default simezone set to UTC
     *
     * Now I use only stubs, so API keys aren't necessary
     *
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        date_default_timezone_set ('UTC');

        parent::__construct($options);
    }

    /**
     * Needed for crate trade select field
     *
     * @return array
     */
    public function getSortedPairs()
    {
        $pairs = array_keys($this->getPrices());
        sort($pairs);
        return $pairs;
    }

    /**
     * Current prices
     *
     * @return array
     */
    public function getPrices()
    {
        $prices = $this->request('ticker/price');
        $pricesArray = [];
        foreach ($prices as $price) {
            $pricesArray[$price['symbol']] = $price['price'];
        }
        return $pricesArray;
    }

    /**
     * For now get only XXX/BTC
     *
     * Potentially costly
     *
     * @return array
     */
    public function getSymbolsWithVolume(): array
    {
        $tickers  = $this->fetch_tickers();
        $symbolsVolume = [];

        foreach ($tickers as $symbol => $data) {
            $isQuoteBtc = explode('/' , $symbol)[1] === 'BTC';
            if ($isQuoteBtc) {
                $symbolsVolume[$symbol] = (int)$data['baseVolume'];
            }
        }

        return $symbolsVolume;
    }

    /**
     * @param string $pair
     * @param string $timeframe
     * @param int $limit
     * @return array
     */
    public function getKlines(string $pair, string $timeframe = '1h', int $limit = 100)
    {
        return $this->fetch_OHLCV($pair, $timeframe, null, $limit);
    }

    /** stub */
    public function buyTrade(Trade $trade)
    {
        $prices = $this->getPrices();
        $buyData = [
            'price' => $prices[$trade->pair]
        ];

        event(new TradeBought($trade, $buyData));
    }

    /** stub */
    public function sellTrade(Trade $trade)
    {
        $prices = $this->getPrices();
        $saleData = [
            'price' => $prices[$trade->pair]
        ];

        event(new TradeSold($trade, $saleData));
    }
}