<?php

namespace App\Console;

use App\Console\Commands\CurrentPricesDataProviderCommand;
use App\Console\Commands\RsiDataProviderCommand;
use App\Console\Commands\TraderCommand;
use App\Helpers\Redis;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /** @var array $commands every command is being run the same way */
        $commands = [
            CurrentPricesDataProviderCommand::NAME,
            RsiDataProviderCommand::NAME,
            TraderCommand::NAME
        ];

        foreach ($commands as $command) {
            $schedule->command($command)
                     ->everyMinute()
                     ->runInBackground()
                     ->appendOutputTo($this->getOutputPath($command))
                     ->when(function () use ($command) {
                         return !$this->isRunning($command);
                     });
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    /**
     * @param string $command
     * @return bool
     */
    private function isRunning(string $command): bool
    {
        /** @var Redis $redis */
        $redis = resolve(Redis::class);

        $lastTimeRun = $redis->getCommandLastTimeRun($command);

        if ($lastTimeRun && $this->isLessThanMinute($lastTimeRun)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $lastTimeRun
     * @return bool
     */
    private function isLessThanMinute(string $lastTimeRun): bool
    {
        return now()->diffInSeconds($lastTimeRun) <= 60;
    }

    /**
     * @param string $command
     * @return string
     */
    private function getOutputPath(string $command): string
    {
        return storage_path("logs/{$command}.log");
    }
}
