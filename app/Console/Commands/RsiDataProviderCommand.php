<?php

namespace App\Console\Commands;

use App\Helpers\Api;
use App\Helpers\Redis;
use Illuminate\Console\Command;

class RsiDataProviderCommand extends Command
{
    public const NAME = 'data-provider:rsi';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = self::NAME;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches data necessary for calculating rsi';

    /**
     * @var Api
     */
    private $api;
    /**
     * @var Redis
     */
    private $redis;

    /**
     * Create a new command instance.
     *
     * @param Api $api
     * @param Redis $redis
     */
    public function __construct(Api $api, Redis $redis)
    {
        parent::__construct();
        $this->api = $api;
        $this->redis = $redis;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        while (true) {
            try {
                $symbolsVolumeArray = $this->api->getSymbolsWithVolume();
            } catch (\Exception $exception) {
                echo $exception->getMessage() . PHP_EOL;
                unset($exception);
            }

            $this->setLastTimeRun();

            sleep(30);

            foreach ($symbolsVolumeArray as $symbol => $volume) {
                $this->redis->setSymbolBaseVolume($symbol, $volume);

                try {
                    $klines = $this->api->getKlines($symbol);
                    $this->redis->setSymbolClosingPrices($symbol, $klines);
                } catch (\Exception $exception) {
                    echo $exception->getMessage() . PHP_EOL;
                    unset($exception);
                }

                usleep(700000);

                try {
                    $klines = $this->api->getKlines($symbol, '15m');
                    $this->redis->setSymbolClosingPrices($symbol, $klines, '15m');
                } catch (\Exception $exception) {
                    echo $exception->getMessage() . PHP_EOL;
                    unset($exception);
                }

                usleep(700000);

                $this->setLastTimeRun();
            }
        }
    }

    private function setLastTimeRun()
    {
        $this->redis->setCommandLastTimeRun(self::NAME, now()->toDateTimeString());
    }
}
