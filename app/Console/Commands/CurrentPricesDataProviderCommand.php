<?php

namespace App\Console\Commands;

use App\Helpers\Api;
use App\Helpers\Redis;
use Illuminate\Console\Command;

class CurrentPricesDataProviderCommand extends Command
{
    public const NAME = 'data-provider:current-prices';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = self::NAME;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets and saves all symbols prices on binance every 5 seconds.';

    /**
     * @var Api
     */
    private $api;

    /**
     * @var Redis
     */
    private $redis;

    /**
     * Create a new command instance.
     *
     * @param Api $api
     * @param Redis $redis
     */
    public function __construct(Api $api, Redis $redis)
    {
        parent::__construct();
        $this->api = $api;
        $this->redis = $redis;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        while (true) {
            $prices = [];

            try {
                $prices = $this->api->getPrices();
            } catch (\Exception $exception) {
                echo $exception->getMessage() . PHP_EOL;
                unset($exception);
            }

            foreach ($prices as $symbol => $price) {
                $this->redis->setCurrentSymbolPrice($symbol, $price);
            }

            $this->redis->setCommandLastTimeRun(self::NAME, now()->toDateTimeString());

            sleep(5);
        }
    }
}
