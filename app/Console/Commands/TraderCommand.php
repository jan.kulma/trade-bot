<?php

namespace App\Console\Commands;

use App\Helpers\Api;
use App\Helpers\Decision;
use App\Helpers\Redis;
use App\Models\Trade;
use Illuminate\Console\Command;

class TraderCommand extends Command
{
    public const NAME = 'run-trader-bot';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = self::NAME;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Iterates over current trades and sells or keeps them.';

    /**
     * @var Decision
     */
    private $decisionHelper;

    /**
     * @var Api
     */
    private $api;

    /**
     * @var Redis
     */
    private $redis;

    /**
     * Create a new command instance.
     *
     * @param Decision $decisionHelper
     * @param Api $api
     * @param Redis $redis
     */
    public function __construct(Decision $decisionHelper, Api $api, Redis $redis)
    {
        parent::__construct();
        $this->decisionHelper = $decisionHelper;
        $this->api = $api;
        $this->redis = $redis;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        while (true) {
            $trades = $this->getUnsoldTrades();
            foreach ($trades as $trade) {
                if ($this->decisionHelper->keep($trade)) {
                    continue;
                }

                $this->api->sellTrade($trade);
            }

            $this->redis->setCommandLastTimeRun(self::NAME, now()->toDateTimeString());

            sleep(5);
        }
    }

    /**
     * Should use repository instead.
     */
    private function getUnsoldTrades()
    {
        return Trade::where('sell_price', null)->get();
    }
}
