<?php

namespace App\Models;

use App\Traits\LogsTradeActions;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where($string, $null)
 */
class Trade extends Model
{
    use LogsTradeActions;

    protected $fillable = [
        'pair', 'note', 'buy_price', 'sell_price', 'take_profit', 'stop_loss'
    ];

    public function isSold()
    {
        return (bool) $this->sell_price;
    }
}
