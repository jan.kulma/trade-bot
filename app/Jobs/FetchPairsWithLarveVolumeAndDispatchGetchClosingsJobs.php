<?php

namespace App\Jobs;

use App\Trading\Helpers\Api;
use App\Trading\Signals\Rsi\DataProvider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class FetchPairsWithLarveVolumeAndDispatchGetchClosingsJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $largeVolume;

    /**
     * @var Api
     */
    private $api;

    /**
     * Create a new job instance.
     *
     * @param int $largeVolume
     */
    public function __construct(int $largeVolume)
    {
        $this->largeVolume = $largeVolume;
        $this->api = resolve(Api::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $pairsWithLargeVolume = $this->api->getPairsWithLargeVolume($this->largeVolume);
        $this->dispatchFetchClosingsJobs($pairsWithLargeVolume);
    }

    private function dispatchFetchClosingsJobs(array $pairsWithLargeVolume)
    {
        foreach ($pairsWithLargeVolume as $pair) {
            $queueLevel = $this->getQueueLevel($pairsWithLargeVolume, $pair);
            FetchClosings::dispatch($pair, DataProvider::TIMEFRAME, $queueLevel, DataProvider::DEFAULT_LIMIT)->delay(now()->addSeconds(60));
        }

        unset($pairsWithLargeVolume);
    }

    private function getQueueLevel(array $pairsWithLargeVolume, string $pair)
    {
        if ($pair === reset($pairsWithLargeVolume)) {
            return FetchClosings::QUEUE_START;
        }

        if ($pair === end($pairsWithLargeVolume)) {
            return FetchClosings::QUEUE_END;
        }

        return FetchClosings::QUEUE_MIDDLE;
    }
}
