<?php

namespace App\Jobs;

use App\Trading\Helpers\Api;
use App\Trading\Helpers\Redis;
use App\Trading\Signals\Rsi\Signal;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Redis as RedisFacade;
use Illuminate\Support\Facades\Log;

class FetchClosings implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const QUEUE_START           = -1;
    const QUEUE_MIDDLE          = 0;
    const QUEUE_END             = 1;

    /** @var string */
    private $pair;

    /** @var Api */
    private $api;

    /** @var string */
    private $timeframe;

    /** * @var int */
    private $queueLevel;

    /** @var Redis */
    private $redis;

    /** @var int */
    private $limit;

    /**
     * Create a new job instance.
     *
     * @param string $pair
     * @param string $timeframe
     * @param int $queueLevel
     * @param int $limit
     */
    public function __construct(string $pair, string $timeframe, int $queueLevel, int $limit)
    {
        $this->limit      = $limit;
        $this->pair       = $pair;
        $this->timeframe  = $timeframe;
        $this->queueLevel = $queueLevel;
        $this->api        = resolve(Api::class);
        $this->redis      = resolve(Redis::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        RedisFacade::throttle('fetch-closings')->allow(5)->every(1)->then(function () {
            if ($this->queueLevel === self::QUEUE_START) $this->onQueueStart();

            try {
                $klines = $this->api->getKlines($this->pair, $this->timeframe, $this->limit);
                $this->redis->saveClosings($klines, $this->pair);
                unset($klines);
            } catch (\Exception $e) {
                Log::error('Failed \App\Jobs\FetchClosings',[
                    'message' => $e->getMessage()
                ]);
            }

            if ($this->queueLevel === self::QUEUE_END) $this->onQueueEnd();
        });
    }

    private function onQueueStart()
    {
        $this->redis->deleteClosings();
        $this->redis->setClosingsStatus(Signal::STATUS_UPDATE);
    }

    private function onQueueEnd()
    {
        $this->redis->setClosingsStatus(Signal::STATUS_READY);
        $this->redis->setClosingsCreatedAt(Carbon::now()->toDateTimeString());
    }
}
