<?php

namespace App\Signals\Rsi;

use App\Helpers\Redis;
use Illuminate\Support\Collection;

class Rsi
{
    /**
     * @var Calculator
     */
    private $calculator;

    /**
     * @var Redis
     */
    private $redis;

    /**
     * Signal constructor.
     * @param Redis $redis
     * @param Calculator $calculator
     */
    public function __construct(Redis $redis, Calculator $calculator)
    {
        $this->calculator = $calculator;
        $this->redis = $redis;
    }

    public function fetch()
    {
        $data1h = $this->getData($this->redis::SYMBOL_1H_CLOSING_PRICES);
        $data1h = $this->process($data1h);
        $data15m = $this->getData($this->redis::SYMBOL_15M_CLOSING_PRICES);
        $data15m = $this->process($data15m);

        return compact('data1h', 'data15m');
    }

    private function getData(string $field): array
    {
        $pattern = '*'.$field.'*';
        $keys = $this->redis->getKeysByPattern($pattern);
        $symbolsData = [];

        foreach ($keys as $key) {
            $symbol = explode($this->redis::GLUE, $key)[0];
            $symbolsData[$symbol]['closingPrices'] = $this->redis->manager->lrange($key, 0, 100);
            $symbolsData[$symbol]['baseVolume'] = $this->redis->getBaseVolume($symbol);
        }

        return $symbolsData;
    }

    private function process(array $data): Collection
    {
        $processedData = [];
        foreach ($data as $symbol => $arrayOfData) {
            $closingPrices = array_map(function ($closingPrice) {
                return [
                    'closingPrice' => $closingPrice
                ];
            }, $arrayOfData['closingPrices']);

            $rsiData = $this->calculator->calculate($closingPrices);
            $rsi = end($rsiData)['rsi']['value'];

            $processedData[$symbol] = [
                'baseVolume' => $arrayOfData['baseVolume'],
                'rsi' => $rsi
            ];
        }

        return collect($processedData)
            ->filter(function ($data) {
                return $data['rsi'] && $data['rsi'] <= 32;
            })
            ->sortBy(function ($data) {
                return $data['rsi'];
            });

    }
}