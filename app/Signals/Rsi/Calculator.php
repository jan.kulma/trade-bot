<?php

namespace App\Signals\Rsi;

class Calculator
{
    /**
     * array = [
     *   0 (day) => [
     *       closingPrice => price,
     *     ]
     *   1 (day) => [
     *       closingPrice => price,
     *     ]
     * ]
     *
     * @param array $data
     * @param int $period
     * @return array
     */
    public function calculate(array $data, int $period = 14)
    {
        $slidingWindowUpMoves = [];
        $slidingWindowDownMoves = [];

        for ($i = 0; $i < count($data); $i++) {

            if ($i < $period) {
                $data[$i]['rsi'] = [
                    'value' => null,
                    'averageUp' => null,
                    'averageDown' => null
                ];
                continue;
            }


            $index = 0;
            for ($j = $i - $period + 1; $j <= $i; $j++) {
                $change = $data[$j]['closingPrice'] - $data[$j - 1]['closingPrice'];

                $slidingWindowUpMoves[$index] = $change > 0 ? $change : 0;
                $slidingWindowDownMoves[$index] = $change < 0 ? abs($change) : 0;
                $index += 1;
            }

            $prevRecord = $data[$i - 1];

            $averageUp = $this->updateAverage($slidingWindowUpMoves, $prevRecord['rsi']['averageUp']);
            $averageDown = $this->updateAverage($slidingWindowDownMoves, $prevRecord['rsi']['averageDown']);

            $value = 100;
            if ($averageDown > 0) {
                $rs = $averageUp / $averageDown;
                $value = 100 - (100 / (1 + $rs));
            }

            $data[$i]['rsi'] = [
                'value' => $value,
                'averageUp' => $averageUp,
                'averageDown' => $averageDown
            ];
        }

        return $data;
    }

    private function updateAverage($changes, $prevAverage)
    {
        if (!is_null($prevAverage)) {
            return $this->wildersSmoothing($changes, $prevAverage);
        }

        return array_sum($changes)/count($changes);
    }

    private function wildersSmoothing($changes, $prevAverage)
    {
        return $prevAverage + ($changes[count($changes) - 1] - $prevAverage) / count($changes);
    }
}