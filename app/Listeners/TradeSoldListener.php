<?php

namespace App\Listeners;

use App\Events\TradeSold;
use App\Http\Repositories\TradeRepository;
use Illuminate\Support\Facades\Redis;

class TradeSoldListener
{
    /**
     * @var TradeRepository
     */
    private $tradeRepository;

    /**
     * @var TradeSold
     */
    private $event;

    /**
     * Create the event listener.
     *
     * @param TradeRepository $tradeRepository
     */
    public function __construct(TradeRepository $tradeRepository)
    {
        $this->tradeRepository = $tradeRepository;
    }

    /**
     * Handle the event.
     *
     * @param  TradeSold  $event
     * @return void
     */
    public function handle(TradeSold $event)
    {
        $this->event = $event;

        $this->updateTrade();
    }

    private function updateTrade()
    {
        $data = [
            'sell_price' => $this->event->saleData['price']
        ];

        $this->tradeRepository->update($this->event->trade, $data);
    }

    private function clearData()
    {
        $trade = $this->event->trade;
        Redis::del('takeProfitsAlreadyReached', "trade.{$trade->id}");
        $trade->log('cleared takeProfitsAlreadyReached');
    }
}
