<?php

namespace App\Listeners;

use App\Events\TradeBought;
use App\Http\Repositories\TradeRepository;

class TradeBoughtListener
{
    /**
     * @var TradeRepository
     */
    private $tradeRepository;

    /**
     * Create the event listener.
     *
     * @param TradeRepository $tradeRepository
     */
    public function __construct(TradeRepository $tradeRepository)
    {
        $this->tradeRepository = $tradeRepository;
    }

    /**
     * Handle the event.
     *
     * @param  TradeBought  $event
     * @return void
     */
    public function handle(TradeBought $event)
    {
        $this->updateTrade($event);
    }

    private function updateTrade(TradeBought $event)
    {
        $data = [
            'buy_price' => $event->buyData['price']
        ];

        $this->tradeRepository->update($event->trade, $data);
    }
}
