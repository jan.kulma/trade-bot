<?php

namespace App\Traits;

use App\Trading\Helpers\Price;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;

trait LogsTradeActions
{
    public function log(string $message)
    {
        $this->saveToLog($message);
    }

    public function createLogFileAndLogBuy()
    {
        $this->checkDirectoryExistence();
        $header = "{$this->now()} bought for: {$this->buy_price}" . PHP_EOL;
        File::put($this->getLogFilePath(), $header);
    }

    public function logSale()
    {
        /** @var Price $priceManager */
        $priceManager = resolve(Price::class);
        $result = $priceManager->getSellPriceToBuyPriceInPercent($this);
        $topPrice = $priceManager->getTopPrice($this);

        $this->saveToLog(
            "{$this->now()} sold for: {$this->sell_price} with result: {$result}. Top price acheived: {$topPrice}."
        );
    }

    public function logPriceChange($percentOfchange, int $currentPrice)
    {
        $change = $percentOfchange > 0 ? '+' : '';
        $change .= $percentOfchange . '%';

        $this->saveToLog(
            "{$this->now()} current price: {$currentPrice} ({$change} to buy price)"
        );
    }

    public function logUpdatedTopPrice($old, $new)
    {
        $this->saveToLog(
            "{$this->now()} updated top price from: {$old} to: {$new}"
        );
    }

    private function saveToLog(string $message)
    {
        File::append(
            $this->getLogFilePath(), $message  . PHP_EOL
        );
    }

    /**
     * @return string
     */
    public function getLogFilePath(): string
    {
        $createdAt = str_replace(['-', ' ', ':'], '_', $this->created_at);
        return "{$this->getLogsDir()}/{$createdAt}_id_{$this->id}.log";
    }

    /**
     * @return string
     */
    private function getLogsDir(): string
    {
        return storage_path('trades');
    }

    private function checkDirectoryExistence(): void
    {
        $dir = $this->getLogsDir();
        if (!File::isDirectory($dir)) {
            File::makeDirectory($dir);
        }
    }

    /**
     * @return Carbon
     */
    private function now(): Carbon
    {
        return  Carbon::now();
    }
}