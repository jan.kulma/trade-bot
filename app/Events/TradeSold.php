<?php

namespace App\Events;

use App\Models\Trade;

class TradeSold
{
    /**
     * @var Trade
     */
    public $trade;
    /**
     * @var array
     */
    public $saleData;

    /**
     * Create a new event instance.
     *
     * @param Trade $trade
     * @param array $saleData
     */
    public function __construct(Trade $trade, array $saleData)
    {
        $this->trade = $trade;
        $this->saleData = $saleData;
    }
}
