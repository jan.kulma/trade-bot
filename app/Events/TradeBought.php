<?php

namespace App\Events;

use App\Models\Trade;

class TradeBought
{
    /**
     * @var Trade
     */
    public $trade;
    /**
     * @var array
     */
    public $buyData;

    /**
     * Create a new event instance.
     *
     * @param Trade $trade
     * @param array $buyData
     */
    public function __construct(Trade $trade, array $buyData)
    {
        $this->trade = $trade;
        $this->buyData = $buyData;
    }
}
