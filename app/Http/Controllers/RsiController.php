<?php

namespace App\Http\Controllers;

use App\Signals\Rsi\Rsi;
use Illuminate\Http\Request;

class RsiController extends Controller
{
    /**
     * @var Rsi
     */
    private $rsi;

    public function __construct(Rsi $rsi)
    {
        $this->rsi = $rsi;
    }

    public function index()
    {
        $data = $this->rsi->fetch();

        return view('signal.rsi', compact('data'));
    }
}
