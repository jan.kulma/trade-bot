<?php

namespace App\Http\Controllers;

use App\Http\Repositories\TradeRepository;
use App\Models\Trade;
use App\Helpers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class TradeController extends Controller
{
    /**
     * @var Api
     */
    private $api;

    /**
     * @var TradeRepository
     */
    private $tradeRepository;

    public function __construct(
        TradeRepository $tradeRepository,
        Api $api
    ) {
        $this->tradeRepository = $tradeRepository;
        $this->api = $api;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trades = $this->tradeRepository->all();

        return view('trade.index', compact('trades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function create()
    {
        $pairs = $this->api->getSortedPairs();

        return view('trade.create', compact('pairs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'pair' => 'required|string',
            'note' => 'nullable|string',
            'take_profit' => 'required|integer',
            'stop_loss' => 'nullable|integer'
        ]);

        $trade = $this->tradeRepository->create($request->all());

        $this->api->buyTrade($trade);

        return redirect('trade/' . $trade->id);
    }

    /**
     * Display the specified resource.
     *
     * @param Trade $trade
     * @return Trade
     */
    public function show(Trade $trade)
    {
        return view('trade.show', compact('trade'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function executeCommand()
    {
        Artisan::call('schedule:run');

        return back();
    }
}
