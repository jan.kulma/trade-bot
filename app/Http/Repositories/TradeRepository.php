<?php

namespace App\Http\Repositories;

use App\Models\Trade;
use App\Helpers\Price;

class TradeRepository
{
    /**
     * @var Trade[]|\Illuminate\Database\Eloquent\Collection
     */
    private $repository;
    /**
     * @var Price
     */
    private $priceHelper;

    public function __construct(Price $priceManager)
    {
        $this->priceHelper = $priceManager;
        $this->repository  = $this->getTrades();
    }

    public function all()
    {
        return $this->repository->sortByDesc('created_at');
    }

    public function recent()
    {
        return $this->repository->sortByDesc('created_at')
            ->take(5);
    }

    public function unsold()
    {
        return $this->repository->where('sell_price', null);
    }

    public function create(array $request)
    {
        /** @var Trade $trade */
        $trade = Trade::create([
            'pair' => $request['pair'],
            'note' => $request['note'],
            'take_profit' => $request['take_profit'],
            'stop_loss' => $request['stop_loss']
        ]);

        return $trade;
    }

    public function update(Trade $trade, array $data)
    {
        /** removes added parameters */
        $trade->refresh();

        $trade->update($data);
    }

    private function getTrades()
    {
        $trades = Trade::all();

        /** @var Trade $trade */
        foreach ($trades as $trade) {
            $resultData = $this->getCurrentResultData($trade);
            $trade->result = $resultData['result'];
            $trade->class = $resultData['class'];
        }

        return $trades;
    }

    private function getCurrentResultData(Trade $trade)
    {
        if ($trade->isSold()) {
            $result = $this->priceHelper->getSellPriceToBuyPriceInPercent($trade);
            if ($result >= 0) {
                $class = 'bg-success';
            } else {
                $class = 'bg-danger';
            }

            return compact('result', 'class');
        }

        $result = $this->priceHelper->getCurrentPriceToBuyPriceInPercent($trade);
        if ($result >= 0) {
            $class = 'table-success';
        } else {
            $class = 'table-danger';
        }

        return compact('result', 'class');
    }
}