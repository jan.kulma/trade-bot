<?php

namespace App\Providers;

use App\Http\Repositories\TradeRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('recentTrades', resolve(TradeRepository::class)->recent());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
