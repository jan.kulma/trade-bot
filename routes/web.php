<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['basicAuth'])->group(function () {
    Route::redirect('/', 'trade');
    Route::get('trade/execute_command', 'TradeController@executeCommand');
    Route::resource('trade', 'TradeController');
    Route::get('signals/rsi', 'RsiController@index');
});