<?php

function btcToSatoshi($btc)
{
    return (int) (((float) $btc) * 100000000);
}