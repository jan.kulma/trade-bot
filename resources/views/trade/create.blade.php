@extends('layout.app')

@section('content')

    <h3>Create new trade</h3>

    <form action="{{ url('trade') }}" method="post">

        @csrf

        <div class="form-group row">
            <div class="col-12 col-sm-2">
                <div class="form-group">
                    <label>Trading pair</label>
                    <select name="pair" class="form-control">
                        @foreach($pairs as $pair)
                            <option value="{{ $pair }}">{{ $pair }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-6 col-sm-1">
                <div class="form-group">
                    <label>Take profit</label>
                    <input name="take_profit" type="text" class="form-control">
                </div>
            </div>
            <div class="col-6 col-sm-1">
                <div class="form-group">
                    <label>Stop loss</label>
                    <input name="stop_loss" type="text" class="form-control">
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-12 col-sm-2">
                <label>Note</label>
                <textarea name="note" class="form-control" rows="5"></textarea>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">Create</button>
    </form>

@endsection