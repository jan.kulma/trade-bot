@extends('layout.app')

@section('content')

<div class="table-responsive">
    <table class="table table-sm">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Pair</th>
                <th scope="col">Result</th>
                <th scope="col">Note</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($trades as $trade)

                <tr>
                    <th scope="row">{{ $trade->id }}</th>
                    <td>{{ $trade->pair }}</td>
                    <td>
                        <span class="{{ $trade->class }} px-1 text-white font-weight-bold">{{ $trade->result }}</span>
                    </td>
                    <td><span class="font-italic">{{ str_limit($trade->note, 10) }}</span></td>
                    <td>
                        <a href="{{ url("trade/{$trade->id}") }}" class="btn btn-link btn-sm">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a>
                    </td>
                </tr>

            @endforeach
        </tbody>
    </table>
</div>



@endsection