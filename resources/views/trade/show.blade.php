@extends('layout.app')

@section('content')

<h3>Trade details</h3>

<p>
    <strong>Note:</strong> <span class="font-italic">{{ $trade->note ?? 'empty' }}</span>
</p>

<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Pair</th>
            <th scope="col">Buy price</th>
            <th scope="col">Sell price</th>
            <th scope="col">Take profit</th>
            <th scope="col">Stop loss</th>
            <th scope="col">Updated at</th>
            <th scope="col">Created at</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">{{ $trade->id }}</th>
                <td>{{ $trade->pair }}</td>
                <td>{{ $trade->buy_price }}</td>
                <td>{{ $trade->sell_price }}</td>
                <td>{{ $trade->take_profit }}</td>
                <td>{{ $trade->stop_loss }}</td>
                <td>{{ $trade->updated_at }}</td>
                <td>{{ $trade->created_at }}</td>
            </tr>
        </tbody>
    </table>
</div>


{{--<h3>Trade log</h3>--}}

{{--<div class="log">--}}
    {{--@foreach(file($trade->getLogFilePath()) as $line)--}}

        {{--{{ $line }} <hr class="my-2">--}}

    {{--@endforeach--}}
{{--</div>--}}

@endsection
