@extends('layout.app')

@section('content')

    <div class="table-responsive">
        <table class="table table-sm">
            <caption><strong>Rsi 1h</strong></caption>
            <thead>
            <tr>
                <th scope="col">Symbol</th>
                <th scope="col">Volume</th>
                <th scope="col">Rsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data['data1h'] as $symbol => $symbolData)

                <tr>
                    <th scope="row">{{ str_replace('/', '', $symbol) }}</th>
                    <td>{{ $symbolData['baseVolume'] }}</td>
                    <td>{{ $symbolData['rsi'] }}</td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>

    <div class="table-responsive">
        <table class="table table-sm">
            <caption><strong>Rsi 15m</strong></caption>
            <thead>
            <tr>
                <th scope="col">Symbol</th>
                <th scope="col">Volume</th>
                <th scope="col">Rsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data['data15m'] as $symbol => $symbolData)

                <tr>
                    <th scope="row">{{ str_replace('/', '', $symbol) }}</th>
                    <td>{{ $symbolData['baseVolume'] }}</td>
                    <td>{{ $symbolData['rsi'] }}</td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>



@endsection