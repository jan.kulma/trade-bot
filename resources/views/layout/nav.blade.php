<nav class="navbar navbar-light bg-paypal fixed-top">
    <div class="btn-group m-1">
        <a class="btn btn-link text-white pr-0" href="{{ url('trade') }}">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </a>
        <button type="button" class="btn btn-link text-white dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
        </button>
        <div class="dropdown-menu">
            <h6 class="dropdown-header">Recent trades</h6>
            @foreach($recentTrades as $trade)
                <a class="dropdown-item" href="{{ url('trade/' . $trade->id) }}">
                    {{ $trade->pair }} <small>{{ $trade->created_at->diffForHumans() }}</small>
                </a>
            @endforeach
        </div>
    </div>

    <a href="{{ url('trade/create') }}" class="btn btn-link text-white m-1">
        <i class="fa fa-plus" aria-hidden="true"></i>
    </a>

    <div class="dropdown m-1">
        <button class="btn btn-link text-white dropdown-toggle underline-none" data-toggle="dropdown">
            <i class="fa fa-question" aria-hidden="true"></i>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <h6 class="dropdown-header">Signals</h6>
            <a href="{{ url('signals/rsi') }}" class="dropdown-item">RSI</a>
        </div>
    </div>

    <div class="dropdown ml-auto mr-1 mt-1 mb-1 menu-more">
        <button class="btn btn-link text-white dropdown-toggle underline-none" type="button" data-toggle="dropdown">
            <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="{{ url('trade/execute_command') }}">Exe cmnd</a>
        </div>
    </div>
</nav>